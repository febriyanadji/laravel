#!/bin/sh
set -e

echo "waiting for database ..."
sleep 2
php artisan migrate --force
php artisan db:seed --force

if [ "${1#-}" != "$1" ]; then
        set -- apache2-foreground "$@"
fi

exec "$@"