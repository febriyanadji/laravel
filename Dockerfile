FROM php:8.2.6-apache

RUN apt-get update && apt-get install git libzip-dev unzip libonig-dev libpng-dev -yqq
RUN docker-php-ext-install zip pdo_mysql mbstring gd

COPY ./apache/000-default.conf /etc/apache2/sites-enabled/
RUN a2enmod rewrite

RUN curl -fsSL https://deb.nodesource.com/setup_lts.x | bash -
RUN apt-get install nodejs -yqq

WORKDIR /var/www/html

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

COPY --chown=www-data:www-data composer.json composer.lock ./

RUN composer install --optimize-autoloader --no-scripts --no-interaction --no-progress

COPY --chown=www-data:www-data package.json package-lock.json ./

RUN npm ci

COPY --chown=www-data:www-data . .

RUN npm run prod

USER www-data

COPY ./scripts/entrypoint.sh /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]

CMD ["apache2-foreground"]